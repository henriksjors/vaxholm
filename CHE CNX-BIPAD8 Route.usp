/*******************************************************************************************
  SIMPL+ Module Information
  (Fill in comments below)
*******************************************************************************************/
/*
Dealer Name:
System Name:
System Number:
Programmer:
Comments:
*/

/*******************************************************************************************
  Compiler Directives
  (Uncomment and declare compiler directives as needed)
*******************************************************************************************/
// #ENABLE_DYNAMIC
// #SYMBOL_NAME ""
// #HINT ""
#DEFINE_CONSTANT	Bipad_MaxIn		24
#DEFINE_CONSTANT	Bipad_MaxOut	24
#DEFINE_CONSTANT	Room_Max		24
#DEFINE_CONSTANT	Source_Max		24
#DEFINE_CONSTANT	Name_Length		50
// #CATEGORY "" 
// #PRINT_TO_TRACE
// #DIGITAL_EXPAND 
// #ANALOG_SERIAL_EXPAND 
// #OUTPUT_SHIFT 
// #HELP_PDF_FILE ""
#DEFAULT_VOLATILE
#ENABLE_STACK_CHECKING
#ENABLE_TRACE
// #ENCODING_ASCII
// #ENCODING_UTF16
// #ENCODING_INHERIT_FROM_PARENT
// #ENCODING_INHERIT_FROM_PROGRAM
/*
#HELP_BEGIN
   (add additional lines of help lines)
#HELP_END
*/

/*******************************************************************************************
  Include Libraries
  (Uncomment and include additional libraries as needed)
*******************************************************************************************/
// #CRESTRON_LIBRARY ""
// #USER_LIBRARY ""

/*******************************************************************************************
  DIGITAL, ANALOG and SERIAL INPUTS and OUTPUTS
  (Uncomment and declare inputs and outputs as needed)
*******************************************************************************************/
// DIGITAL_INPUT 
// ANALOG_INPUT 
// STRING_INPUT 
// BUFFER_INPUT 

DIGITAL_INPUT	Room_Is_On[Room_Max];
DIGITAL_INPUT	Room_Mute_On[Room_Max];
DIGITAL_INPUT	Room_Mute_Off[Room_Max];
DIGITAL_INPUT	Room_Loudness_On[Room_Max];
DIGITAL_INPUT	Room_Loudness_Off[Room_Max];
DIGITAL_OUTPUT	Room_Mute_On_Fb[Room_Max];
DIGITAL_OUTPUT	Room_Loudness_On_Fb[Room_Max];
DIGITAL_OUTPUT	Bipad_Out_Room_On[Bipad_MaxOut]; 
DIGITAL_OUTPUT	Bipad_Out_Mute[Bipad_MaxOut]; 
DIGITAL_OUTPUT	Bipad_Out_Loudness[Bipad_MaxOut]; 
ANALOG_INPUT	Room_Source[Room_Max];
ANALOG_INPUT	Room_Volume[Room_Max];
ANALOG_INPUT	Room_Balance[Room_Max];
ANALOG_INPUT	Room_Bass[Room_Max];
ANALOG_INPUT	Room_Treble[Room_Max];
ANALOG_INPUT	Room_MaxVol[Room_Max];
ANALOG_INPUT	Room_MinVol[Room_Max];
STRING_INPUT	Room_Name$[Room_Max][Name_Length];
STRING_INPUT	Room_Source_Name$[Room_Max][Name_Length];
ANALOG_INPUT	Source_Gain[Source_Max];
STRING_INPUT	Source_Name$[Source_Max][Name_Length];

ANALOG_OUTPUT	Bipad_Out_Source[Bipad_MaxOut]; 
ANALOG_OUTPUT	Bipad_Out_Volume[Bipad_MaxOut]; 
ANALOG_OUTPUT	Bipad_Out_Balance[Bipad_MaxOut]; 
ANALOG_OUTPUT	Bipad_Out_Bass[Bipad_MaxOut]; 
ANALOG_OUTPUT	Bipad_Out_Treble[Bipad_MaxOut];
ANALOG_OUTPUT	Bipad_Out_MaxVol[Bipad_MaxOut]; 
ANALOG_OUTPUT	Bipad_Out_MinVol[Bipad_MaxOut];
ANALOG_OUTPUT	Bipad_In_Source_Comp[Bipad_MaxIn]; 
ANALOG_OUTPUT	Source_Gain_Fb[Source_Max]; 
// STRING_OUTPUT 

/*******************************************************************************************
  SOCKETS
  (Uncomment and define socket definitions as needed)
*******************************************************************************************/
// TCP_CLIENT
// TCP_SERVER
// UDP_SOCKET

/*******************************************************************************************
  Parameters
  (Uncomment and declare parameters as needed)
*******************************************************************************************/
// INTEGER_PARAMETER
// SIGNED_INTEGER_PARAMETER
// LONG_INTEGER_PARAMETER
// SIGNED_LONG_INTEGER_PARAMETER
STRING_PARAMETER	Bipad_Out_Name$[Bipad_MaxOut][Name_Length];
STRING_PARAMETER	Bipad_In_Name$[Bipad_MaxIn][Name_Length];

/*******************************************************************************************
  Parameter Properties
  (Uncomment and declare parameter properties as needed)
*******************************************************************************************/
/*
#BEGIN_PARAMETER_PROPERTIES parameter_variable, parameter_variable, ...
   // propValidUnits = // unitString or unitDecimal|unitHex|unitPercent|unitCharacter|unitTime|unitTicks;
   // propDefaultUnit = // unitString, unitDecimal, unitHex, unitPercent, unitCharacter, unitTime or unitTicks;
   // propBounds = lower_bound , upper_bound;
   // propDefaultValue = ;  // or, propDefaultValue = "";
   // propList = // { "value" , "label" } , { "value" , "label" } , ... ;
   // propShortDescription = "status_bar_hint_text";
   // #BEGIN_PROP_FULL_DESCRIPTION  line_1...  line_2...  line_n  #END_PROP_FULL_DESCRIPTION
   // #BEGIN_PROP_NOTES line_1...  line_2...  line_n  #END_PROP_NOTES
#END_PARAMETER_PROPERTIES
*/

/*******************************************************************************************
  Structure Definitions
  (Uncomment and define structure definitions as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: struct.myString = "";
*******************************************************************************************/
/*
STRUCTURE MyStruct1
{
};

MyStruct1 struct;
*/

/*******************************************************************************************
  Global Variables
  (Uncomment and declare global variables as needed)
  Note:  Be sure to initialize all declared STRING variables as needed
         For example, in Function Main: myString = "";
*******************************************************************************************/
INTEGER	iBipad_Out_Room[Bipad_MaxOut];
Nonvolatile INTEGER	iBipad_Out_Room_On[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Mute[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Loudness[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Source[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Volume[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Balance[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Bass[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_Treble[Bipad_MaxOut];
Nonvolatile INTEGER	iBipad_Out_MaxVol[Bipad_MaxOut]; 
Nonvolatile INTEGER	iBipad_Out_MinVol[Bipad_MaxOut];
Nonvolatile INTEGER iRoom_Mute[Room_Max];
Nonvolatile INTEGER iRoom_Loudness[Room_Max];

// LONG_INTEGER
// SIGNED_INTEGER
// SIGNED_LONG_INTEGER
// STRING

/*******************************************************************************************
  Functions
  (Add any additional functions here)
  Note:  Functions must be physically placed before the location in
         the code that calls them.
*******************************************************************************************/

Function Update_Room(integer iRoom)
{
	Integer i;
	for (i = 1 to Bipad_MaxOut)
	{
		if(iBipad_Out_Room[i] = iRoom)
		{
			Bipad_Out_Room_On[i] 	= Room_Is_On[iRoom];
			Bipad_Out_Mute[i] 		= iRoom_Mute[iRoom];
			Bipad_Out_Loudness[i] 	= iRoom_Loudness[iRoom];
			Bipad_Out_Source[i] 	= Room_Source[iRoom];
			Bipad_Out_Volume[i] 	= Room_Volume[iRoom];
			Bipad_Out_Balance[i]	= Room_Balance[iRoom];
			Bipad_Out_Bass[i]		= Room_Bass[iRoom];
			Bipad_Out_Treble[i] 	= Room_Treble[iRoom];
			Bipad_Out_MaxVol[i] 	= Room_MaxVol[iRoom];
			Bipad_Out_MinVol[i] 	= Room_MinVol[iRoom];
           	
		}		
	}
}

/*
Integer_Function MyIntFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return (0);
}
*/

/*
String_Function MyStrFunction1()
{
    // TODO:  Add local variable declarations here

    // TODO:  Add code here

    Return ("");
}
*/

/*******************************************************************************************
  Event Handlers
  (Uncomment and declare additional event handlers as needed)
*******************************************************************************************/

PUSH Room_Mute_On
{
	iRoom_Mute[GetLastModifiedArrayIndex()] = 1;
	Room_Mute_On_Fb[GetLastModifiedArrayIndex()] = 1;
}
                  
PUSH Room_Mute_Off
{
	iRoom_Mute[GetLastModifiedArrayIndex()] = 0;
	Room_Mute_On_Fb[GetLastModifiedArrayIndex()] = 0;
}

PUSH Room_Loudness_On
{
	iRoom_Loudness[GetLastModifiedArrayIndex()] = 1;
	Room_Loudness_On_Fb[GetLastModifiedArrayIndex()] = 1;
}
                  
PUSH Room_Loudness_Off
{
	iRoom_Loudness[GetLastModifiedArrayIndex()] = 0;
	Room_Loudness_On_Fb[GetLastModifiedArrayIndex()] = 0;
}
/*
RELEASE input
{
    // TODO:  Add code here
}
*/

CHANGE Room_Is_On
{
	integer i;
	for(i = 1 to Bipad_MaxOut)
	{
		if(iBipad_Out_Room[i] = GetLastModifiedArrayIndex())
		{
			Bipad_Out_Room_On[i] = Room_Is_On[GetLastModifiedArrayIndex()];
			iBipad_Out_Room_On[i] = Room_Is_On[GetLastModifiedArrayIndex()];
		}
	}
	
}

CHANGE Room_Source
{
	integer i;
	for(i = 1 to Bipad_MaxOut)
	{
		if(iBipad_Out_Room[i] = GetLastModifiedArrayIndex())
		{
			Bipad_Out_Source[i] = Room_Source[GetLastModifiedArrayIndex()];
			iBipad_Out_Source[i] = Room_Source[GetLastModifiedArrayIndex()];
		}
	}
	
}

CHANGE ROOM_NAME$
{
	integer i;
	for(i = 1 to Bipad_MaxOut)
	{
		if(ROOM_NAME$[GetLastModifiedArrayIndex()] = Bipad_Out_Name$[i])
		{
			iBipad_Out_Room[i] = GetLastModifiedArrayIndex();
			Update_Room(GetLastModifiedArrayIndex());
		}
	}
}

/*
EVENT
{
    // TODO:  Add code here
}
*/

/*
SOCKETCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETDISCONNECT
{
    // TODO:  Add code here
}
*/

/*
SOCKETRECEIVE
{
    // TODO:  Add code here
}
*/

/*
SOCKETSTATUS
{
    // TODO:  Add code here
}
*/

/*******************************************************************************************
  Main()
  Uncomment and place one-time startup code here
  (This code will get called when the system starts up)
*******************************************************************************************/
/*
Function Main()
{
    // TODO:  Add code here
    // Initialize declared global and local variables/arrays as needed.

    // WaitForInitializationComplete();
    // If you are reading any Input or Output variables, uncomment
    //   the WaitForInitializationComplete statement above and read
    //   them afterwards.  Input/Output variables will not have
    //   their correct values set until after the logic processor
    //   runs and propagates the values to them.
}
*/

